import {Button} from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function ArchiveProduct({product_id, fetchProducts, isActive}) {
	

	const archiveProduct = (productId) => {
		fetch(`${process.env.REACT_APP_API_URL}/api/products/${productId}/archive`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(response => response.json())
		.then(result => {
			if(result){
				Swal.fire({
					title: 'Product has been archived',
					text: 'Product has been archived successfully',
					icon: 'success'
				})
				fetchProducts()
			} else {
				Swal.fire({
					title: 'Somthing wen wrong',
					text: 'PLease try again',
					icon: 'error'
				})
				fetchProducts();
			}
		})
	}
	

	const activateProduct = (productId) => {
		fetch(`${process.env.REACT_APP_API_URL}/api/products/${productId}/activate`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(response => response.json())
		.then(result => {
			if(result){
				Swal.fire({
					title: 'Product has been activated',
					text: 'Product has been activated successfully',
					icon: 'success'
				})
				fetchProducts()
			} else {
				Swal.fire({
					title: 'Somthing wen wrong',
					text: 'PLease try again',
					icon: 'error'
				})
				fetchProducts();
			}
		})
	}


	return(
		<>
			{isActive ?
				<Button 
					variant='warning' 
					size="sm"
					onClick={() => archiveProduct(product_id)}> Archive
				</Button>
			:
				<Button 
				variant='success' 
				size='sm'
				onClick={() => activateProduct(product_id)}> Activate 
				</Button>
			}
		</>
	)
}