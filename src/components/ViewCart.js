import { useState, useEffect, useContext } from 'react';
import { Container, Row, Col, Card, Button } from 'react-bootstrap';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';
import PropTypes from 'prop-types';
import CreateOrderModal from '../components/CreateOrderModal';
import { Navigate } from 'react-router-dom';

export default function ViewCart({ cartData, fetchCart }) {
    const { user } = useContext(UserContext);

    const handleRemoveFromCart = (productId) => {
        console.log('user.id', user.id);
        console.log('user', user);
        if (!user || !user.id) {
            Swal.fire({
                title: 'Login required',
                text: 'Please log in to remove items from your cart.',
                icon: 'warning',
            });
            return;
        }

        // Remove the item from the cartData array instead of setting it in state
        const updatedCartData = cartData.filter(item => item.product._id !== productId);

        fetch(`${process.env.REACT_APP_API_URL}/api/users/remove-from-cart/${productId}`, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`,
            },
        })
            .then(response => response.json())
            .then(result => {
                if (result.message === 'Product removed from cart') {
                    Swal.fire({
                        title: 'Product removed',
                        text: 'The product has been removed from your cart.',
                        icon: 'success',
                    });

                    fetchCart()
                    
                 
                } else {
                    Swal.fire({
                        title: 'Error',
                        text: 'Something went wrong. Please try again.',
                        icon: 'error',
                    });
                }
            })
            .catch(error => {
                console.error('Error removing product from cart:', error);
            });
    };

    return (
        <Container className='py-5'>
            <h1 className="my-4 cardBody NavbarBrand-text">Your Cart</h1>
            {cartData.length === 0 ? (
                <p>Your cart is empty.</p>
            ) : (
                <Row xs={1} md={2} lg={3} className="g-4">
                    {cartData.map((item, index) => (
                        <Col key={index}>
                            <Card>
                                <Card.Img variant="top" src={item.product.image} />
                                <Card.Body>
                                    <Card.Title>{item.product.name}</Card.Title>
                                    <Card.Text>
                                        Quantity: {item.quantity}
                                        <br />
                                        Subtotal: Php {item.subtotal}
                                    </Card.Text>
                                    <Button
                                        variant="danger"
                                        onClick={() => handleRemoveFromCart(item.product._id)}
                                    >
                                        Remove from Cart
                                    </Button>
                                </Card.Body>
                            </Card>
                        </Col>
                    ))}
                </Row>
            )}
        </Container>
    );
}

ViewCart.propTypes = {
    cartData: PropTypes.array.isRequired,
};
