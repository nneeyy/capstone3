import {useState, useEffect, useContext} from 'react';
import {Form, Button, Modal} from 'react-bootstrap';
import {Navigate, useNavigate} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext.js';
import { useModalContext } from '../ModalContext';


export default function AddProduct({fetchProducts}) {
  //  Initialization
  const navigate = useNavigate();
  const {user} = useContext(UserContext);
  const {showAddProductModal, closeModal, modalType} = useModalContext();

  // States
  const [image, setImage] = useState();
  const [name, setName] = useState("")
  const [description, setDescription] = useState("")
  const [price, setPrice] = useState(0)
  const [quantity, setQuantity] = useState(1)


  // Functions 
  function createProduct(event){
    // Insert course creation Logic here

    // Prevents the default behavior of page relaod when submitting a form
    event.preventDefault();

    let token = localStorage.getItem('token');

    // Fetch function to the course creationg API 
    fetch(`${process.env.REACT_APP_API_URL}/api/products/`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`
      },
      body: JSON.stringify({
        name: name,
        description: description,
        price: price,
        image: image,
        quantity: quantity
      })
    })
    .then(response => response.json())
    .then(result => {
      if(result){
        Swal.fire({
          title: 'New Product Added',
          icon: 'success'
        })

        // For clearing the form  
        setName("")
        setDescription("")
        setPrice("")
        setImage("")
        setQuantity("")
        closeModal();

        // Redirect to /courses after creation of a new course
        
        navigate('/products')
      } else {
        Swal.fire({
          title: 'Something went wrong',
          text: 'Product creation unsuccessful',
          icon: 'error'
        })
      }
    })
  }


  return(
    
         <Modal show={showAddProductModal} onHide={closeModal}>
            <Form onSubmit={(event) => createProduct(event)}>
            <Modal.Header closeButton>
             <Modal.Title> Add Product </Modal.Title>
            </Modal.Header>

            <Modal.Body>
                   <Form.Group>
                        <Form.Label>Url for image:</Form.Label>
                        <Form.Control 
                        type="url" 
                        placeholder="Enter url" 
                        required value={image || ''}  onChange={event => {setImage(event.target.value)}}/>
                    </Form.Group>

                    <Form.Group>
                        <Form.Label>Name:</Form.Label>
                        <Form.Control 
                        type="text" 
                        placeholder="Enter Name" 
                        required value={name} onChange={event => {setName(event.target.value)}}/>
                    </Form.Group>

                    <Form.Group>
                        <Form.Label>Description:</Form.Label>
                        <Form.Control type="text" placeholder="Enter Description" required value={description} onChange={event => {setDescription(event.target.value)}}/>
                    </Form.Group>

                    <Form.Group>
                        <Form.Label>Price:</Form.Label>
                        <Form.Control type="number" placeholder="Enter Price" required value={price} onChange={event => {setPrice(event.target.value)}}/>
                    </Form.Group>

                    <Form.Group>
                        <Form.Label>Quantity:</Form.Label>
                        <Form.Control type="number" placeholder="Enter Quantity" required value={quantity} onChange={event => {setQuantity(event.target.value)}}/>
                    </Form.Group>

              </Modal.Body>

              <Modal.Footer> 
                  <Button 
                  variant="primary" 
                  type="submit" 
                  className="my-5">
                  Submit
                  </Button>
              </Modal.Footer>
            </Form>
          </Modal>
      
  )
}