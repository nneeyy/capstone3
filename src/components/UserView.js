import React, {useState, useEffect} from 'react';
import {Row} from 'react-bootstrap';
import ProductCard from './ProductCard.js';


export default function UserView({productsData}) {
	const [products, setProducts] = useState([])

	useEffect(() => {
		if (productsData) {
      // Filters out the products to only be products that are active
      const activeProducts = productsData.map((product) => {
        if (product.isActive === true) {
          return <ProductCard product={product} key={product._id} />;
        } else {
          return null;
        }
      });
      // Set the courses state to the active courses array
      setProducts(activeProducts);
    }
	}, [productsData])

	return(
		<>
			<h1 className='text-center' data-aos="fade-down" data-aos-delay="100" data-aos-duration="1000"> ITEMS </h1>
      <Row className='justify-content-center' data-aos="fade-right" data-aos-delay="100" data-aos-duration="1000">
			{ products }
      </Row>
		</>
	)
}