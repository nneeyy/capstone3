import {Row, Col, Card, Button, Container, image, Carousel} from 'react-bootstrap';
import {NavLink, Link} from 'react-router-dom';
import myImage from '../image/ps4.jpg';
import myImage2 from '../image/ff7.png';
import myImage3 from '../image/red.jpg';
import myImage4 from '../image/hogwarts.jpg';
import myImage5 from '../image/Gow.jpg';
import myImage6 from '../image/Loz.jpg';
import myImage7 from '../image/mh.jfif';
import myImage8 from '../image/ps5(1).jpg';
import myImage9 from '../image/switch.png';

export default function Highlights() {
	return (
	
		<Row> 
			<Col xs={12} md={4} className='my-2' data-aos="fade-right" data-aos-delay="100" data-aos-duration="1000"> 
				<Card className="cardHighlight p-3 carousel-control">
				 <Carousel slide={false}>
				      <Carousel.Item>
				        <img 
				        src={myImage}
				        width = "450"
				        height = "450"
				        className="d-inline-block  w-100 h-50 image-fluid carousel-control"
				        />
				      </Carousel.Item>

				      <Carousel.Item>
				       <img 
				        src={myImage2}
				        width = "450"
				        height = "450"
				        className="d-inline-block  w-100 h-50 image-fluid carousel-control"
				        />
				      </Carousel.Item>

				      <Carousel.Item>
				       <img 
				        src={myImage3}
				        width = "450"
				        height = "450"
				        className="d-inline-block w-100 h-50 carousel-control"
				        />
				      </Carousel.Item>

				    </Carousel>
				    <Card.Body className="cardBody">
				        <Card.Title 
				        className="NavbarBrand-text">
				            <h2>
				            PS4 Games
				            </h2>
				        </Card.Title>

				        <Button 
				          className="paraFont" 
				          variant="primary"
						  as={NavLink}
						  to='/products'>
				          Check Here
				          </Button>

				    </Card.Body>
				        
				</Card>
			</Col>

			<Col xs={12} md={4} className='my-2' data-aos="fade-up" data-aos-delay="100" data-aos-duration="1000"> 
				<Card className="cardHighlight p-3 carousel-control">
				<Carousel slide={false}>
				      <Carousel.Item>
				        <img 
				        src={myImage8}
				        width = "450"
				        height = "450"
				        className="d-inline-block  w-100 h-50 image-fluid carousel-control"
				        />
				      </Carousel.Item>

				      <Carousel.Item>
				       <img 
				        src={myImage4}
				        width = "450"
				        height = "450"
				        className="d-inline-block  w-100 h-50 image-fluid carousel-control"
				        />
				      </Carousel.Item>

				      <Carousel.Item>
				       <img 
				        src={myImage5}
				        width = "450"
				        height = "450"
				        className="d-inline-block w-100 h-50 carousel-control"
				        />
				      </Carousel.Item>

				    </Carousel>
				    <Card.Body className="cardBody">
				        <Card.Title 
				        className="NavbarBrand-text">
				            <h2>
				            PS5 Games
				            </h2>
				        </Card.Title>

				          <Button 
				          className="paraFont" 
				          variant="primary"
						  as={NavLink}
						  to='/products'>
				          Check Here 
				          </Button>
				    </Card.Body>
				</Card>
			</Col>

			<Col xs={12} md={4} className='my-2' data-aos="fade-left" data-aos-delay="100" data-aos-duration="1000"> 
				<Card className="cardHighlight p-3 carousel-control">
				<Carousel slide={false}>
				      <Carousel.Item>
				        <img 
				        src={myImage9}
				        width = "450"
				        height = "450"
				        className="d-inline-block  w-100 h-50 image-fluid carousel-control"
				        />
				      </Carousel.Item>

				      <Carousel.Item>
				       <img 
				        src={myImage6}
				        width = "450"
				        height = "450"
				        className="d-inline-block  w-100 carousel-control"
				        />
				      </Carousel.Item>

				      <Carousel.Item>
				       <img 
				        src={myImage7}
				        width = "450"
				        height = "450"
				        className="d-inline-block w-100 h-50 image-fluid carousel-control"
				        />
				      </Carousel.Item>

				    </Carousel>
				    <Card.Body className="cardBody">
				        <Card.Title 
				        className="NavbarBrand-text">
				            <h2>
				            Switch Games
				            </h2>
				        </Card.Title>

				          <Button 
				          className="paraFont" 
				          variant="primary"
						  as={NavLink}
						  to='/products'>
				          Check Here 
				          </Button>
				    </Card.Body>
				</Card>
			</Col>

			
		</Row>
	)
	
	
}