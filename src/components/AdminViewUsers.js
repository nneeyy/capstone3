import { Table } from 'react-bootstrap';
import { useState, useEffect } from 'react';
import SetAsAdmin from './SetAsAdmin';

export default function AdminViewUsers({ usersData, fetchUsers }) {
  const [users, setUsers] = useState([]);

  useEffect(() => {
    const usersArray = usersData.map(user => (
      <tr key={user._id}>
        <td>{user._id}</td>
        <td>{user.firstName} {user.lastName}</td>
        <td>{user.mobileNo}</td>
        <td>{user.email}</td>
        <td>{user.password}</td>
        <td>{user.isAdmin ? 'Admin' : 'User'}</td>
        {/* Other user-specific columns */}
        <td>
        <SetAsAdmin 
                user_id={user._id} 
                fetchUsers={fetchUsers} 
                isAdmin={user.isAdmin}
                />
        </td>
        <td>Delete Action </td>
      </tr>
    ));

    setUsers(usersArray);
  }, [usersData]);

  return (
    <>
      <h1 className="NavbarBrand-text text-center p-5">Admin Dashboard</h1>
      <h2 className="text-center">Users</h2>
      <Table striped bordered hover responsive size='sm'>
        <thead>
          <tr>
            <th className="text-center">ID</th>
            <th className="text-center">Name</th>
            <th className="text-center">Mobile Number</th>
            <th className="text-center">Email</th>
            <th className="text-center">Password</th>
            <th colSpan={2} className="text-center">Admin Status</th>
            {/* Other user-specific headers */}
            <th className="text-center">Actions</th>
          </tr>
        </thead>

        <tbody>{users}</tbody>
      </Table>
    </>
  );
}