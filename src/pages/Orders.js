import { useEffect, useState, useContext } from 'react';
import AdminViewOrders from '../components/AdminViewOrders.js';
import UserContext from '../UserContext.js';
import { Navigate } from 'react-router-dom';

export default function Orders() {
const {user} = useContext(UserContext);
  const [orders, setOrders] = useState([]);

  const fetchOrders = () => {
    fetch(`${process.env.REACT_APP_API_URL}/api/orders/all`)
      .then(response => response.json())
      .then(result => {
        setOrders(result);
      })
      .catch(error => {
        console.error('Error fetching orders:', error);
      });
  };

  useEffect(() => {
    fetchOrders();
  }, []);

  return (
   
        (user.isAdmin === true)?
            <AdminViewOrders ordersData={orders} fetchUsers={fetchOrders}/>    
        :
            <Navigate to='/'/> 
  
  )
}