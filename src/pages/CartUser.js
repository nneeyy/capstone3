import { useEffect, useState, useContext } from 'react';
import UserContext from '../UserContext';
import ViewCart from '../components/ViewCart';
import { Navigate } from 'react-router-dom';

export default function CartUser() {
    const { user } = useContext(UserContext);
    const [cartData, setCartData] = useState([]);

    const fetchCart = () => {
        if (user.id) {
            fetch(`${process.env.REACT_APP_API_URL}/api/users/cart/${user.id}`, {
                headers: {
                    Authorization: `Bearer ${localStorage.getItem('token')}`,
                },
            })
                .then(response => response.json())
                .then(result => {
                    setCartData(result.cart);
                    localStorage.setItem('cartData', JSON.stringify(result.cart)); // Store cart data in localStorage
                })
                .catch(error => {
                    console.error('Error fetching cart data:', error);
                });
        } else {
            console.log(user.id);
        }
    }

    useEffect(() => {
        const storedCartData = localStorage.getItem('cartData');
        if (storedCartData) {
            setCartData(JSON.parse(storedCartData)); // Retrieve cart data from localStorage
        } else {
            fetchCart();
        }
        // fetchCart();
    }, []);

    return <ViewCart cartData={cartData} fetchCart={fetchCart} />;
}