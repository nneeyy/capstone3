import { useEffect, useState, useContext } from 'react';
import AdminViewUsers from '../components/AdminViewUsers';
import UserView from '../components/UserView';
import UserContext from '../UserContext.js';
import { Navigate, NavLink } from 'react-router-dom';

export default function Users() {
const {user} = useContext(UserContext);
  const [users, setUsers] = useState([]);

  const fetchUsers = () => {
    fetch(`${process.env.REACT_APP_API_URL}/api/users/all`)
      .then(response => response.json())
      .then(result => {
        setUsers(result);
      })
      .catch(error => {
        console.error('Error fetching users:', error);
      });
  };

  useEffect(() => {
    fetchUsers();
  }, []);

  return (
     
      (user.isAdmin === true) ?
              <AdminViewUsers usersData={users} fetchUsers={fetchUsers}/> 
          : 
              <Navigate to='/' />   
    
  )
}